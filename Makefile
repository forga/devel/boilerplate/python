VBIN=${VIRTUAL_ENV}/bin

help: # Print help on Makefile
	@grep '^[^.#]\+:\s\+.*#' Makefile | \
	sed "s/\(.\+\):\s*\(.*\) #\s*\(.*\)/`printf "\e[1;33;4;40m"`\1`printf "\033[0m"`	\3/" | \
	expand -t20

clean: # Remove files not tracked in source control
	find . -type f -name "*.orig" -delete
	find . -type f -name "*.pyc" -delete
	find . -type d -name "__pycache__" -delete
	find . -type d -empty -delete

format: # Format the code and lint it
	${VBIN}/black *.py
	.git/hooks/pre-commit

init-pre_commit: # Set up git pre-commit hook
	echo "make --no-print-directory --quiet lint" > .git/hooks/pre-commit && chmod u+x .git/hooks/pre-commit

lint: # Lint code
	${VBIN}/black --quiet --check *.py && echo "✅ black" || echo "🚨 black"
	${VBIN}/pflake8 --config=pyproject.toml && echo "✅ pflake8"  || echo "🚨 pflake8"
	${VBIN}/pydocstyle && echo "✅ pydocstyle"  || echo "🚨 pydocstyle"
	${VBIN}/pylint --rcfile=pyproject.toml *.py && echo "✅ pylint"  || echo "🚨 pylint"

open_all: # Open all projects files
	${EDITOR} ${VBIN}/activate
	${EDITOR} .gitlab-ci.yml main.py Makefile pyproject.toml README.md requirements-dev.txt requirements.txt
	${EDITOR} .git/hooks/p*-commit

pre_commit: # Run the pre-commit hook
	bash .git/hooks/pre-commit

test: # Run the code tests
	./main.py -h 1> /dev/null && echo ✅ || echo 🚨
